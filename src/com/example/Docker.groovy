#!/usr/bin/env groovy
package com.example

class Docker implements Serializable {
    def script

    Docker(script) {
        this.script = script
    }

    def buildDockerImage(String imageName) {
        script.echo "Building Docker image for $imageName"
        script.withCredentials([script.usernamePassword(credentialsId: 'dockerhub', passwordVariable: 'PWD', usernameVariable: "USR")]) {
            script.sh "docker build -t $imageName ."
            script.sh "echo $script.PWD | docker login -u $script.USR --password-stdin"
            script.sh "docker push $imageName"
        }
    }
}