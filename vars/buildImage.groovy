#!/usr/bin/env groovy

def call(String imageName){
    echo "Building Docker image for $imageName"
    withCredentials([usernamePassword(credentialsId: 'dockerhub',passwordVariable: 'PWD',usernameVariable: "USR")]) {
    sh "docker build -t $imageName ."
    sh "echo $PWD | docker login -u $USR --password-stdin"
    sh "docker push $imageName"

    }
}
